#include <linux/fs.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/spi/spi.h>
#include <linux/uaccess.h> 
#include <linux/gpio.h>

#define DEVICE_NAME "joydrive"
#define  CLASS_NAME  "MISC"     
#define CHIP_SELECT_PIN 8 


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anthony Kolenic 201580508");
MODULE_DESCRIPTION("A driver for a joystick module to communicate with a raspberry pi");

//Parameters
static int maxSpeed = 1000;
module_param(maxSpeed,int,S_IRUGO);
MODULE_PARM_DESC(maxSpeed,"Maximum speed slave device can handle, i.e. max speed of ADC for joystick");

static int busNum = 0;
module_param(busNum,int,S_IRUGO);
MODULE_PARM_DESC(busNum,"Bus number to use, find by listing /sys/class/spi_master");

static int chipSelect = 0;
module_param(chipSelect,int,S_IRUGO);
MODULE_PARM_DESC(chipSelect,"Chip select number to use");

//Global variables
static int majorNum;
static int channel = 0;
static struct spi_device *spi_device;
static struct proc_dir_entry* procDir;

//Promises
static int device_open(struct inode*, struct file*);
static int device_release(struct inode*,struct file*);
static ssize_t device_read(struct file*, char* ,size_t,loff_t*);
static ssize_t device_write(struct file*,const char* ,size_t,loff_t*);


//Helper Methods Promises
static int read_byte_mcp3008(int channel);
static int create_SPI_device(void);
static int bind_GPIO_pin(void);

static struct file_operations fops =
{
	.owner = THIS_MODULE,
	.open = device_open,
	.release = device_release,
	.read = device_read,
	.write = device_write
};

static int __init joydrive_init(void)
{
	printk(KERN_INFO "JoyDrive: Entered initialization code, Attempting to register character device \n");
	majorNum = register_chrdev(0, DEVICE_NAME, &fops);
   if (majorNum<0){
      printk(KERN_ALERT "JoyDrive: Failed to register a major number\n");
      return majorNum;
   }
   printk(KERN_INFO "JoyDrive: Registration successful, assigned major number is %d\n", majorNum);
 
	procDir = proc_create(DEVICE_NAME,0,NULL,&fops);
	  
 
	if (create_SPI_device() != 0)
	{
		return -1;
	}
	
	if (bind_GPIO_pin() != 0)
	{
		return -1;
	}
	
	printk(KERN_INFO "JoyDrive: Initialization was successful\n");
	/*
	//Code to attempt to read from mcp3008
	char buffer[40]; // buffer is 40 as LCM of 8 and 10 is 40
	buffer[0] = 0b1;
	buffer[1] = 0b00001000 << 4;
	if(spi_write(spi_device,buffer,1)< 0)
	{
		printk(KERN_ALERT "JoyDrive: Failed to write to slave.\n");
	}
	
	if(spi_read(spi_device,buffer,2) < 0)
	{
		printk(KERN_ALERT "JoyDrive: Failed to read from slave.\n");
	}
	printk("a: %d b: %d\n",buffer[0],buffer[1]);
	*/
	
	return 0;
}

static void __exit joydrive_exit(void)
{
	unregister_chrdev(majorNum, DEVICE_NAME); 
	proc_remove(procDir);
	if( spi_device )
	{
		spi_unregister_device( spi_device );
	}
	gpio_free(CHIP_SELECT_PIN);
	printk(KERN_INFO "JoyDrive: Exiting");
}

static int device_open(struct inode* inodep, struct file* filep)
{
	printk(KERN_INFO "JoyDrive: Opening character device\n");
	return 0;	
}

static int device_release(struct inode* inodep,struct file* filep)
{
	printk(KERN_INFO "JoyDrive: Closing character device\n");
	return 0;
}

static ssize_t device_read(struct file* filep, char* buffer ,size_t length ,loff_t* offset)
{
	printk(KERN_INFO "JoyDrive: Reading from character device\n");
	int value = read_byte_mcp3008(channel);
	printk(KERN_INFO "JoyDrive: Offset: %d\n", offset);
	if (*offset > 0) return 0; 
	if (value >= 0)
	{
		char temp_buffer[100];
		if (length < 100) //their buffer is smaller than ours, stop because we could cause an overflow for them
			return 0;
		int size = sprintf(temp_buffer,"%d",value) + 1;
		temp_buffer[size] = 0;
		printk(KERN_ALERT "JoyDrive: writing %d bytes to user\n",size);
		if (copy_to_user(buffer,temp_buffer,size))
			return -EFAULT;
		*offset = size;
		return size;  
			
	}
	else
	{
		printk(KERN_ALERT "JoyDrive: Failed to read value from channel : %d\n",channel);
	}
	
	return 0;
}

static ssize_t device_write(struct file* filep,const char* buffer ,size_t length ,loff_t* offset)
{
	printk(KERN_INFO "JoyDrive: Writing from character device\n");
	char temp_buffer[100];
	if (length > 100)
		return -EFAULT;
	if (copy_from_user(temp_buffer,buffer,length))
		return -EFAULT;
	int newChannel;
	if (sscanf(temp_buffer,"%d",&newChannel) != 1)
	{
		printk(KERN_ALERT "JoyDrive: Something weird happened: %s\t%d \n",temp_buffer,newChannel);
		return -EFAULT;
	}	
	
	if (newChannel >= 0 && newChannel < 8)
		channel = newChannel;
		//move offset
	printk(KERN_ALERT "New channel: %d\n",channel);
	int temp = strlen(buffer);
	*offset = temp;
	return temp;
}

static int create_SPI_device(void)
{
	struct spi_board_info spi_slave_info = {
        .modalias = "joydrive",
        .max_speed_hz = maxSpeed,
        .bus_num = busNum,
        .chip_select = chipSelect,
        .mode = 3,
    }; 
    
	struct spi_master * master = spi_busnum_to_master( busNum);
 	if (!master)
 	{
 		printk(KERN_ALERT "JoyDrive: Failed to find master.\n");
 		return -1;
 	}
	printk(KERN_INFO "JoyDrive: Successfully found master device information\n");
	
	spi_device = spi_new_device( master, &spi_slave_info );
 
	if( !spi_device ) 
	{
		printk(KERN_ALERT "JoyDrive: Failed to create slave from given data.\n");
		return -1;
	}


   spi_device->bits_per_word = 8;
   printk(KERN_INFO "JoyDrive: Successfully created spi device\n");
   
	if( spi_setup( spi_device ))
	{
		printk(KERN_ALERT "JoyDrive: Failed to setup slave.\n");
		spi_unregister_device( spi_device );
		return -1;
	}
	
	printk(KERN_INFO "JoyDrive: Successfully setup spi device\n");
	return 0;
}

static int bind_GPIO_pin(void)
{
	if (gpio_request(CHIP_SELECT_PIN, "ChipSel"))
	{
		printk(KERN_ALERT "JoyDrive: Failed to bind to chip select gpio.\n");
		return -1;
	}
	printk(KERN_INFO "JoyDrive: GPIO chip select binding was successful\n");
	if (gpio_direction_output(CHIP_SELECT_PIN, 1))// initialize pin to on
	{
		printk(KERN_ALERT "JoyDrive: Failed to to initialize chip select to 0.\n");
		return -1;
	}
	printk(KERN_INFO "JoyDrive: GPIO chip select initialization was successful\n");
	return 0;
}

static int read_byte_mcp3008(int channel)
{
	int result  = -1;
	if (channel >= 0 && channel < 8) //mcp3008 only has 8 channels
	{
		//Pull chip low
		gpio_set_value(CHIP_SELECT_PIN,0);
		ssize_t returnVal = 0;
		spi_w8r8(spi_device,0x01); //start transfer
		char command = (0x08 | channel) << 4; //select channel
		returnVal = spi_w8r8(spi_device,command);
		if (returnVal < 0) return -1;
		result = returnVal & 0x03; // keep last two bits 
		result = result << 8; // move to not overwrite as result will be 10 bit number		
		returnVal = spi_w8r8(spi_device,0xFF); //send garbage to retrieve last 8 bits
		if (returnVal < 0) return -1;
		result |= returnVal;
		
		//Pull chip high
		gpio_set_value(CHIP_SELECT_PIN,1);
	}
	return result;
}

module_init(joydrive_init);
module_exit(joydrive_exit);