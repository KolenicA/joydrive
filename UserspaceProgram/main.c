#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

void printMenu()
{
	printf("Menu:\n");
	printf("a - Read Channel\n");
	printf("b - Read Joystick Values\n");
	printf("q - exit\n");
}

int main(void)
{
	//increase the size by one to always allow a zero to be inserted
	char buffer[100];
	int fileDesc = open("/proc/joydrive",O_RDWR);
	if (fileDesc < 0)
	{
		printf("Failed to open driver\n");
		return -1;
	}
	int canExit = 0;
	char input = 0;
	while (!canExit)
	{
		printMenu();
		scanf(" %c",&input);
		
		switch (input)
		{
			case 'A':
			case 'a':
			{
				int newChannel = 0;
				printf("Please enter channel id to read from:\n");
				scanf(" %d",&newChannel);
				int size = sprintf(buffer,"%d",newChannel);
				if (size > 0)
				{
					//go to start of file
					lseek(fileDesc,0,SEEK_SET);
					//switch channel
					write(fileDesc,buffer,size);	
	
				 	//go to start of file
					lseek(fileDesc,0,SEEK_SET);
					int numRead =read(fileDesc,buffer,100);
					printf("Number Read: %d\n",numRead);
					buffer[numRead] = 0;
					printf("value: %s\n",buffer);
				}
				break;
			}
			case 'B':
			case 'b':
			{
				lseek(fileDesc,0,SEEK_SET);
				write(fileDesc,"0\0",2);	
				lseek(fileDesc,0,SEEK_SET);
				int numRead =read(fileDesc,buffer,100);
				printf("Number Read: %d\n",numRead);
				buffer[numRead] = 0;
				printf("x: %s",buffer);
	
				lseek(fileDesc,0,SEEK_SET);
				write(fileDesc,"1\0",1);
	
				lseek(fileDesc,0,SEEK_SET);
   			read(fileDesc,buffer,100);
   			buffer[numRead] = 0;
				printf(" y: %s\n",buffer);
				
				break;
			}
			case 'q':
			case 'Q':
			{
				canExit = 1;
				break;
			}
			default:
			{
				printf("Unknown menu option \"%c\"\n",input);
				break;
			}
		}
	}

	
	close(fileDesc);
	return 0;
}